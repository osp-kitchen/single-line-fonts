## Stories

A.V. Hershey, Calligraphy For Computers, 1967  
[Hershey calligraphy for computers](https://archive.org/details/hershey-calligraphy_for_computers)

Frank Griesshammer Hershey Fonts history  
[The Hershey Fonts with Frank Griesshammer](https://vimeo.com/178015110) **Vimeo link**
In the 1960s, at Dahlgren Naval Weapons Laboratory, Dr. Allen V. Hershey worked on what was to become some of the earliest digital fonts. Hershey’s work is impressive for both technical and creative aspects, and was documented in a report called “CALLIGRAPHY FOR COMPUTERS”. Discovering this paper inspired Frank Grießhammer to resurrect Hershey’s work and bring the fonts to the age of OpenType. Frank will talk about the history and environment of Hershey’s creation, and touch on the current state of resurrection. He has talked on the topic at TypeCon 2015 before, and while some parts will overlap, the presentation will not be identical.


*El alfabeto se encuentra con la Megalópolis* https://www.youtube.com/watch?v=Usc4gmqZ8YY

*Drawing curved* http://drawingcurved.osp.kitchen

Florian Hardwig & Thomas Maier  
From Lettering Guides to CNC Plotters — A Brief History of Technical Lettering Tools
https://www.typotheque.com/articles/from_lettering_guides_to_cnc_plotters


## Tools

### Extensions

[Inkscape strokefont de Shriinivas](https://github.com/Shriinivas/inkscapestrokefont)

[Hersheytextjs de Techninja](https://github.com/techninja/hersheytextjs)

### Recipes

[OSP Ume Stroke Stroke](https://cloud.osp.kitchen/s/w69DHKmSyzij5rS)

OSP Ume Plume (à venir)


## Fonts


### Stroke fonts for real

Single Line Fonts (site)  
https://www.singlelinefonts.com/collections/svg-fonts
https://www.singlelinefonts.com/collections/rhino-fonts

One Line Fonts  
https://www.onelinefonts.com/


Glukfonts.pl (single-line fonts TTF / Fontforge export)  
https://www.glukfonts.pl/font.php?font=ResamitzSL-SVGinOT
https://www.glukfonts.pl/font.php?font=ZnikoSL-SVGinOT-8

Carvalho-Bernau, police de caractères Bill, 2013   
(2 graisses, hommage au Gill Sans, meta-squelette pour générateur de fontes automatiques en ligne pour le Sandberg Institute)
https://sandberg.nl/typeface


Team Rietveld aussi: Paul Bernhard https://pbernhard.com
et sa Hershey-futural qui est très cool: https://gitlab.com/swrs 

### Drawn in single-lines

[DINDong, Clara Sambot](https://typotheque.genderfluid.space/DINdong.html)

[Ductus, Amélie Dumont](https://typotheque.genderfluid.space/ductus.html)

[Routed Gothic](https://webonastick.com/fonts/routed-gothic/samples.html)

### Research

[Burrowlab](https://www.instagram.com/burrowlab/)


