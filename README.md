# Single-lines — Toulouse / Bruxelles


![Saul Steinberg, ‹Sans Titre›, The New Yorker, 4 décembre 1965](img/intro/single-line_etat_des_lieux_isdat_cambre_erg_02.jpg)
*Saul Steinberg, ‹Sans Titre›, The New Yorker, 4 décembre 1965*

## Première rencontre
Mercredi 30 novembre 2022 17:00—19:00
—
échange en ligne «Polices de caractères single-line, un état des lieux isdaT, La Cambre erg»

### Contexte
Avant l'apparition de la typographie vers 1450 en Occident, l'expérience de l'écriture était en contraste une pratique extrêmement variée (entre la caroline rapide des scribes et la capitale gravée dans le marbre des romains) et unifiée autour du trait et de son ductus. Le poinçon qui a commencé à dessiner des caractères par leur bord (leur extérieur) dans des matières dures destinées à produire en série des lettres en plomb a introduit une manière indirecte de comprendre la lettre. Et ainsi, depuis plus de cinq siècles, la typographie a été industriellement tenue séparée de l'écriture. Depuis la genèse de la typographie digitale, cette séparation a organiquement été maintenue, en partie par la révérence de la loge des typographes se sentant dépositaires d'une très longue tradition, en partie par souci de commodité par les grands éditeurs logiciels par ailleurs aux prises avec l'effroyable chantier de la gestion encore non entièrement résolue des très nombreux systèmes d'écritures existant dans le monde. Le standard Opentype 1.8 ouvre, par la petite porte, discrètement, un moyen pour produire des fontes dessinées par le trait. Et entame, par ce biais, le chemin de réconciliation entre typographie digitale et écriture.  
Notre objectif est de découvrir, d'imaginer ou de développer des moyens de rendre les stroke fonts utilisables et stylisables dans divers scénarios, tels que les pages web, les outils de conception basés sur des canevas, ainsi que les traceurs à plume, les environnements de conception CNC, PCP et cartographiques.  
Ce premier échange en ligne est l'occasion de partager des expériences, recettes et interfaces. Un répertoire de travail sera mis en route pour rassembler liens et ressources. En février à l'occasion d'une deuxième rencontre live à Bruxelles, du 6 au 10 février, un workshop permettra de mettre en pratique une sélection de pistes et de publier les ressources au format web.

### Conference room
Big Blue Button
live @https://bbb.erg.school/b/lud-toq-wpo-stg


### Programme

**17h**  
accueil

**17h10**  
François Chastanet (isdaT)  
Un historique des expérimentations typographiques menées à l'isdaT ayant conduit au développement de projets d'alphabets filaires pour les environnements CNC / fablab, notamment le projet Relief SingleLine et ses développements actuels autour de la gravure laser et des tables traçantes grand format (multicouches, feutres, marqueurs gouache, etc.). Un rapide panorama des formats et méthodologies d'export des polices de caractères «open path» sera ici présenté afin de transmettre un processus de conception et d'usage fluide et simplifié ouvert à toutes et tous.

**17h40**  
break

**17h50**   
Open Source Publishing : Pierre Huyghebaert (La Cambre) & Ludi Loiseau (erg)

- Up pen down stories 
http://osp.kitchen/live/up-pen-down/
- Metapost workshops series
    - Erg Meta elastique 
https://gitlab.com/erg-type/workshop.meta-elastique
Metapost anrt 
https://gitlab.constantvzw.org/osp/workshop.metapost-anrt
- Custom tools and recipies
Plancton https://gitlab.com/Luuse/plancton
- Ume Plume et Ume Stroke stroke 
https://gitlab.constantvzw.org/osp/residency.besancon/-/blob/main/pdfs/stroke-stroke.pdf
- Usages (catalogue de stroke fonts en usage et contextes)

**18h10 → 19h**   
Questions, table ronde et débat


![image issue d'un forum questions fablab en ligne](img/intro/single-line_etat_des_lieux_isdat_cambre_erg_01.jpg)
*image issue d'un forum questions fablab en ligne*
